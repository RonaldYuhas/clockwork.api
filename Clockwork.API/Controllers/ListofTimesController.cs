﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clockwork.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class ListofTimesController : Controller
    {
        // GET: api/<controller>/
        [HttpGet]
        public IActionResult Get()
        {
            List<CurrentTimeQuery> returnVal = new List<CurrentTimeQuery>();
            using (var db = new ClockworkContext())
            {
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    returnVal.Add(CurrentTimeQuery);
                }
            }

            return Ok(returnVal);
        }
    }
}
