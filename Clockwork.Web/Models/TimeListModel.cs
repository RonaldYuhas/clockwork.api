﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;

namespace Clockwork.Web.Models
{
    public class TimeListModel
    {
        public List<CurrentTimeQuery> Times { get; private set; } = new List<CurrentTimeQuery>();
        public ReadOnlyCollection<TimeZoneInfo> TimeZones { get; private set; }
        public string SelectedZone { get; set; }
        public TimeListModel()
        {
            GetAllTimeEntries();
            PupulateTimeZones();
        }

        private void PupulateTimeZones()
        {
            TimeZones = TimeZoneInfo.GetSystemTimeZones();
        }

        private void GetAllTimeEntries()
        {
            List<CurrentTimeQuery> result = null;
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString("http://127.0.0.1:5000/api/listoftimes");

                }

                catch (Exception) { }
                result = !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<List<CurrentTimeQuery>>(json_data) : new List<CurrentTimeQuery>();
            }
            Times = result;
        }
            public IEnumerator<CurrentTimeQuery> GetEnumerator()
            {
                return Times.GetEnumerator();
            }
        }
    }
    public class CurrentTimeQuery
    {
        public int CurrentTimeQueryId { get; set; }
        public DateTime Time { get; set; }
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
    }
